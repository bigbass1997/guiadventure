package tm.info.bigbass1997.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.Debug;
import tm.info.bigbass1997.MainDisplay;
import tm.info.bigbass1997.managers.ShapeManager;
import tm.info.bigbass1997.objects.GUI;
import tm.info.bigbass1997.objects.item.Item;

public class GamePlayState extends BasicGameState {

	/*
	 * GUIAdventure is a text-rpg style game but adds in more GUIs.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	public static Item mouseItem;
	public static GUI gui;
	
	int stateID = -1;

	public GamePlayState(int stateID) {
		this.stateID = stateID;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		gui = new GUI();
		gui.init(gc, sbg);
		
		Debug.logInfo("GamePlayState Initialization Method Loaded!");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.fill(ShapeManager.Rect(g, 0, 0, MainDisplay.SWIDTH, MainDisplay.SHEIGHT, 0xFFFFFFFF)); // Background
		
		gui.render(gc, sbg, g);
		
		if(mouseItem != null && mouseItem.getImage() != null) g.drawImage(mouseItem.getImage(), gc.getInput().getMouseX() - (mouseItem.getImage().getWidth() / 2), gc.getInput().getMouseY() - (mouseItem.getImage().getHeight() / 2));
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		gui.update(gc, sbg, delta);
	}
	
	@Override
	public int getID() {
		return stateID;
	}
}
