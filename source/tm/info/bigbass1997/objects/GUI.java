package tm.info.bigbass1997.objects;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.managers.ShapeManager;

public class GUI {

	public static Player player;
	
	public GUI(){
		
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		player = new Player();
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		ShapeManager.Rect(g, 10, 10, gc.getWidth() - 20, gc.getHeight() - 20, 0xFF999999, 2);
		ShapeManager.Rect(g, 20, 20, 420, 300, 0xFF666666, 2);
		ShapeManager.Rect(g, 450, 20, 330, 300, 0xFF666666, 2);
		
		System.out.println(player.getMaxHealth().getValue());
		System.out.println(player.getHealth().getValue());
		System.out.println("TOTAL: " + (player.getMaxHealth().getValue() / player.getHealth().getValue()));
		ShapeManager.FillBar(g, 30, 30, 100, 10, (player.getHealth().getValue() / player.getMaxHealth().getValue()) * 100, 0xFFFFFFFF, 0xFF00FF00);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		
	}
}
