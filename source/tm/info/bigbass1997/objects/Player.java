package tm.info.bigbass1997.objects;

public class Player {
	
	/*
	 * GUIAdventure is a text-rpg style game but adds in more GUIs.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	private Stat health, maxHealth, mana, attack, defence, wisdom, exp, neededExp, level;
	
	public Player(){
		level = new Stat("level", 1.0f);
		exp = new Stat("exp", 0.0f);
		neededExp = new Stat("neededExp", 100.0f);
		
		attack = new Stat("attack", 1.0f);
		defence = new Stat("defence", 1.0f);
		wisdom = new Stat("wisdom", 1.0f);

		maxHealth = new Stat("maxHealth", 100.0f);
		health = new Stat("health", maxHealth.getValue());
		mana = new Stat("mana", 1.0f);
		
		updateStats();
	}
	
	private void updateStats(){
		maxHealth.setValue((float) Math.ceil(level.getValue() * (defence.getValue() * 1.25f)));
		mana.setValue((float) Math.ceil(wisdom.getValue() * 1.075f));
	}
	
	public void grandExp(int exp){
		this.exp.setValue(this.exp.getValue() + exp);
		
		if(this.exp.getValue() >= neededExp.getValue()){
			level.setValue(level.getValue() + 1);
			this.exp.setValue(this.exp.getValue() - neededExp.getValue());
		}
	}
	
	public Stat getHealth(){
		return health;
	}
	
	public Stat getMaxHealth(){
		return maxHealth;
	}
}
