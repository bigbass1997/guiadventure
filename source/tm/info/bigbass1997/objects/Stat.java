package tm.info.bigbass1997.objects;

public class Stat {
	
	/*
	 * GUIAdventure is a text-rpg style game but adds in more GUIs.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	private String name;
	private float value;
	
	public Stat(String name, float value){
		this.name = name;
		this.value = value;
	}
	
	public String getName(){
		return name;
	}
	
	public float getValue(){
		return value;
	}
	
	public void setValue(float val){
		value = val;
	}
}
