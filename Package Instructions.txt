=== ECLIPSE ===
1. Export Project
2. Export as JAR file (NOT RUNNABLE!!)
3. Uncheck libs folder
4. Uncheck .classpath & .project files
5. Check only "Export generated class files and resources" & "Export Java source files and resources"
6. Select the export destination
7. Options: Check the first 2, "Compress the contents..." & "Add directory entries"
8. Select "Generate the Manifest file"
9. Type in the Main Class entry point
10. FINISH!

=== JarSplice ===
 - JARS -
1. Add LIBs Jars
2. Add previously exported JAR file from eclipse

 - NATIVES -
1. Add respective native files for the required OS of the release (ex. Linux Jar use Linux Natives)

 - MAIN CLASS -
1. Type in the Main Class entry point

 - CREATE FATJAR -
1. Create the final Jar file for the game/software

=== WinRAR ===
1. Open final JAR file in WinRAR
2. Create DIR named "source"
3. Create DIR inside "source" called the name of the software/game. (ex. BaseCode)
4. Copy the resources folder into the previously created folder.

FINISHED!